import './app.css';
import Home from "./Home/Home"
import {motion} from 'framer-motion'

function App() {
  return <motion.div
    initial={{opacity: 0}}
    animate={{opacity: 1}}
    exit={{opacity: 0, transition:{duration: 0.5}}}
  >
    <Home/>
  </motion.div>
}

export default App;
