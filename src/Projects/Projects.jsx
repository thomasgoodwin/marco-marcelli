import "./styles.css"
import "../BackgroundSolid/styles.css"
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { useNavigate } from "react-router-dom";
import { motion } from 'framer-motion'

const Projects = () => {
  let navigate = useNavigate();

  return <motion.div
    className="full-screen"
    initial={{ opacity: 0 }}
    animate={{ opacity: 1 }}
    exit={{ opacity: 0 }}
  >
    <Row>
      <Col xs={10}>
        <div id="projects-header">PROJECTS</div>
      </Col>
      <Col xs={2}>
        <div id="about-button" onClick={() => {
          navigate("/about")
        }}>ABOUT</div>
      </Col>
    </Row>
    <Row style={{ marginTop: "75px" }}>
      <Col xs={2} />
      <Col xs={4}>
        <div className="Thumbnail-container">
          <div id="Narrative" className="Thumbnail" />
          <button id="Narrative-inner" className="Thumbnail-inner" onClick={() => {
            navigate("/projects/narrative-design")
          }}>
            Narrative Level <br />Design in Unreal
          </button>
        </div>
      </Col>
      <Col xs={4}>
        <div className="Thumbnail-container">
          <div id="GameDesign" className="Thumbnail" />
          <button id="GameDesign-inner" className="Thumbnail-inner" onClick={() => {
            navigate("/projects/game-design")
          }}>
            Game Design
          </button>
        </div>
      </Col>
      <Col xs={2} />
    </Row>
    <Row style={{ marginTop: "125px" }}>
      <Col xs={2} />
      <Col xs={4}>
        <div className="Thumbnail-container">
          <div id="Arch480" className="Thumbnail" />
          <button id="Arch480-inner" className="Thumbnail-inner" onClick={() => {
            navigate("/projects/unreal")
          }}>
            Houses on the <br />Edge of Caldera: <br />Unreal Engine Render
          </button>
        </div>
      </Col>
      <Col xs={4}>
        <div className="Thumbnail-container">
          <div id="Arch400" className="Thumbnail" />
          <button id="Arch400-inner" className="Thumbnail-inner" onClick={() => {
            navigate("/projects/beacon")
          }}>
            Beacon: A Lantern to <br />Guide the Lost
          </button>
        </div>
      </Col>
      <Col xs={2} />
    </Row>
    <Row style={{ marginTop: "125px" }}>
      <Col xs={2} />
      <Col xs={4}>
        <div className="Thumbnail-container">
          <div id="Arch302" className="Thumbnail" />
          <button id="Arch302-inner" className="Thumbnail-inner" onClick={() => {
            navigate("/projects/food-center")
          }}>
            Urban Food Center
          </button>
        </div>
      </Col>
      <Col xs={4}>
        <div className="Thumbnail-container">
          <div id="Arch401" className="Thumbnail" />
          <button id="Arch401-inner" className="Thumbnail-inner" onClick={() => {
            navigate("/projects/sculpted-curiosity")
          }}>
            Sculpted Curiosity:<br /> A Persistent
          </button>
        </div>
      </Col>
      <Col xs={2} />
    </Row>
    <Row style={{ marginTop: "125px" }}>
      <Col xs={2} />
      <Col xs={4}>
        <div className="Thumbnail-container">
          <div id="Personal" className="Thumbnail" />
          <button id="Personal-inner" className="Thumbnail-inner" onClick={() => {
            navigate("/projects/personal-work")
          }}>
            Personal Work
          </button>
        </div>
      </Col>
      <Col xs={4}>
      </Col>
      <Col xs={2} />
    </Row>
    <Row style={{ marginTop: "75px" }} />
  </motion.div>
}

export default Projects;