import "typeface-roboto";
import "@fontsource/questrial";
import "./styles.css"
import { useNavigate } from "react-router-dom";

import doc2 from  "../assets/Arch302/pdf/Marcelli_P6-02.jpg";
import doc3 from  "../assets/Arch302/pdf/Marcelli_P6-03.jpg";
import doc4 from  "../assets/Arch302/pdf/Marcelli_P6-04.jpg";
import doc5 from  "../assets/Arch302/pdf/Marcelli_P6-05.jpg";
import doc6 from  "../assets/Arch302/pdf/Marcelli_P6-06.jpg";
import doc7 from  "../assets/Arch302/pdf/Marcelli_P6-07.jpg";
import doc8 from  "../assets/Arch302/pdf/Marcelli_P6-08.jpg";
import doc9 from  "../assets/Arch302/pdf/Marcelli_P6-09.jpg";
import doc10 from "../assets/Arch302/pdf/Marcelli_P6-10.jpg";
import doc11 from "../assets/Arch302/pdf/Marcelli_P6-11.jpg";
import doc12 from "../assets/Arch302/pdf/Marcelli_P6-12.jpg";
import doc13 from "../assets/Arch302/pdf/Marcelli_P6-13.jpg";
import doc14 from "../assets/Arch302/pdf/Marcelli_P6-14.jpg";
import doc15 from "../assets/Arch302/pdf/Marcelli_P6-15.jpg";
import doc16 from "../assets/Arch302/pdf/Marcelli_P6-16.jpg";
import doc17 from "../assets/Arch302/pdf/Marcelli_P6-17.jpg";

import thumbnail1 from "../assets/Arch400/final.jpg";
import thumbnail2 from "../assets/Arch480/Cover.jpg";
import thumbnail3 from "../assets/BoardGames/board_game_top_down.jpg"
import thumbnail4 from "../assets/Arch302/urbanfoodcenter.jpg"
import thumbnail5 from "../assets/Arch401/Library.png"
import thumbnail6 from "../assets/PersonalWork/Repetition.jpg"


const Home = () => {
  let navigate = useNavigate();

  return <>
    <div
       className='sr-only'
       aria-hidden='true'
    >
      {[doc2, doc3, doc4, doc5, doc6, doc7, doc8, doc9, doc10, doc11, doc12, doc13, doc14, doc15, doc16, doc17,
        thumbnail1, thumbnail2, thumbnail3, thumbnail4, thumbnail5, thumbnail6].map(src => (
        <img
          alt=''
          src={src}
        />
      ))}
    </div>

    <div className="background-image"/>
    <div id="home-name">Marco Marcelli</div>
    <p id="home-title">Architect for Games</p>
    <button id="projects-home-button" onClick={()=>{
      navigate("/projects")
    }}> 
      PROJECTS
    </button>
    <button id="about-home-button" onClick={()=>{
      navigate("/about")
    }}>
      ABOUT
    </button>
  </>
}


export default Home;