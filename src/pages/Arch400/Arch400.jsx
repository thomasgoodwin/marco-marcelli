import "../../BackgroundSolid/styles.css";
import Navbar from "../../Navbar/Navbar"
import {motion} from 'framer-motion'
import "./styles.css"
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import img1 from '../../assets/Arch400/longfinal.jpg'
import img2 from '../../assets/Arch400/renderOutside.jpg'
import img3 from '../../assets/Arch400/axonWithLights.jpg'
import img4 from '../../assets/Arch400/floorPlan.jpg'
import img5 from '../../assets/Arch400/figure4.jpg'
import { useNavigate } from "react-router-dom";

const Arch400 = () => {
  let navigate = useNavigate();
  return <motion.div 
    className="full-screen"
    initial={{opacity: 0}}
    animate={{opacity: 1}}
    exit={{opacity: 0}}
  >
    <Navbar header1="Beacon: A Lantern to Guide the Lost"/>
    
    <Row style={{marginTop: "125px"}} className="Arch400-row">
      <Col xs={1}/>
      <Col xs={4}>
        <img style={{height: "300px"}}className="Arch400-img" src={img1}/>
      </Col>
      <Col xs={1}/>
      <Col xs={4}>
        <p className="Arch400-page-text">
          A climate disaster multi-use project located in Seattle Proper, on the waterfront of Lake Union. Programs divided by floor, the entire building built upon large columns with an open public space underneath for community activity. 
        </p>
      </Col>
      <Col xs={1}/>
    </Row>
    <Row className="Arch400-row">
      <Col xs={1}/>
      <Col xs={4}>
        <p className="Arch400-page-text">
          Solar shade units have voltaic devices, shading from harsher heat in summers while powering the building in case of disaster. Precast concrete systems reinforce the building for seismic forces that may hit the coast nearby. 
        </p>
      </Col>
      <Col xs={1}/>
      <Col xs={4}>
        <img className="Arch400-img" src={img2}/>
      </Col>
      <Col xs={1}/>
    </Row>
    <Row className="Arch400-row">
      <Col xs={1}/>
      <Col xs={4}>
        <img className="Arch400-img" src={img3}/>
      </Col>
      <Col xs={1}/>
      <Col xs={4}>
        <p className="Arch400-page-text">
          An 18-foot 2nd story allows for large-scale flooding and water level increase without compromising the factory or residential areas above. A housing and worker coop operates and owns a bike manufacturing warehouse on the second floor. The third floor has a row of residential units, split into two-story family spaces, and a smaller one-story unit. 
        </p>
      </Col>
      <Col xs={1}/>
    </Row>
    <Row style={{marginTop: "100px"}}>
      <img className="Arch400-img-big" src={img4}/>
    </Row>
    <Row style={{marginTop: "100px", marginBottom: "80px"}}>
      <img className="Arch400-img-big" src={img5}/>
    </Row>
    <Row style={{ marginBottom: "80px"}}>
      <Col xs={1}/>
      <Col xs={3}>
        <div role="button" className="navbar-button" onClick={()=>{
          navigate("/projects/unreal")
        }}>
          ← UNREAL RENDER
        </div>
      </Col>
      <Col xs={4}/>
      <Col xs={4}>
        <div role="button" className="navbar-button" onClick={()=>{
          navigate("/projects/food-center")
        }}>
          URBAN FOOD CENTER →
        </div>
      </Col>
    </Row>
  </motion.div>
}

export default Arch400;