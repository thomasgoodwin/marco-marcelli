import "../../BackgroundSolid/styles.css";
import Navbar from "../../Navbar/Navbar";
import { motion } from "framer-motion";
import "./styles.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import img1 from "../../assets/BoardGames/board_game_top_down.jpg";
import img2 from "../../assets/BoardGames/prototypePicA.jpg";
import img3 from "../../assets/BoardGames/prototypePicB.jpg";
import img4 from "../../assets/BoardGames/prototypePicC.jpg";
import img5 from "../../assets/BoardGames/prototypePicD.jpg";
import { useNavigate } from "react-router-dom";

const GameDesign = () => {
  let navigate = useNavigate();
  return (
    <motion.div
      className="full-screen"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <Navbar header1="Game Design" />

      <Row
        style={{ padding: "0px 20px", marginTop: "125px" }}
        className="GameDesign-row"
      >
        <Col xs={2} />
        <Col style={{ overflow: "hidden" }} xs={4}>
          <img className="GameDesign-img" src={img2} />
        </Col>
        <Col xs={4}>
          <h2
            className="GameDesign-page-text"
            style={{
              fontSize: "48px",
              textAlign: "center",
              marginBottom: "20px",
            }}
          >
            Steel Sunrise
          </h2>
          <p className="GameDesign-page-text">
            <p>
              You are a scavenger in a distant future ravaged by climate disaster,
              tasked with looting fallen mechs for scraps and other supplies
              during the ongoing war. You’ve just heard the crashing of an
              important robot, containing a processing unit that could either
              power your entire refugee colony or fetch enough money to bribe them
              into a local city. It’s clear you need to set out immediately
              because 3 neighboring encampments heard it too.
            </p>
            <p>
              A tabletop board game for 2-4 players, ranging from 30-60 minutes
              per game. Players engage with looting, stamina, combat, and map
              encounter mechanics.
            </p>
            <p>
              Each player's goal is to break into the mech at the map’s center and
              safely return to your starting camp with its CPU. Other map loot is
              important and is also used to decide the game’s winner after the
              endgame is met.
            </p>
          </p>
        </Col>
        <Col xs={2} />
      </Row>
      <Row
        style={{ padding: "0px 20px", marginTop: "60px" }}
        className="GameDesign-row"
      >
        <Col xs={2} />
        <Col xs={4}>
          <p className="GameDesign-page-text">
            <h2
              className="GameDesign-page-text"
              style={{
                fontSize: "32px",
                textAlign: "center",
                marginBottom: "20px",
              }}
            >
              Creative Process
            </h2>
            <p>
              The main idea I wanted to execute in this game was the concept of
              inertia and momentum, that a player could build up speed and lose it
              quickly. While running fast and building speed would gain you an
              advantage, but if you tripped or stopped moving, that would impact
              you harder than if you were moving slow.
            </p>
            There were some mechanics I wanted to take advantage of to get this
            theme across:
            <ul>
              <li>
                Adding a die to rolls when momentum is built, and removing dice
                when a player encounters a setback
              </li>
              <li>
                Cards that gain increasing advantages based off of momentum
                levels{" "}
              </li>
              <li>
                Obstacles and setbacks that will increase based off momentum{" "}
              </li>
            </ul>
            Important factors I considered when going into this project:
            <ul>
              <li>How to make gameplay rewarding</li>
              <li>Pace of game and how turns are done/ordered</li>
              <li>Starting player isn’t unfairly advantaged</li>
              <li>Balance of randomness vs. strategy</li>
              <li>
                Possible playstyle for high risk high reward and safer
                strategies
              </li>
            </ul>
          </p>
        </Col>
        <Col style={{ overflow: "hidden" }} xs={4}>
          <img className="GameDesign-img" src={img1} />
        </Col>
        <Col xs={2} />
      </Row>
      <Row
        style={{ padding: "0px 20px", marginTop: "60px" }}
        className="GameDesign-row"
      >
        <Col xs={3} />
        <Col xs={6}>
          <h2
            className="GameDesign-page-text"
            style={{
              fontSize: "48px",
              textAlign: "center",
              marginBottom: "20px",
            }}
          >
            Runway
          </h2>
          <p className="GameDesign-page-text">
            Runway is a card game focused on hand curation with a focus on
            people, personality, and fashion. Players utilize game mechanics to
            curate the best possible hand and are faced with difficult choices.
            Each turn, a player draws a card and a die is rolled to determine an
            auction, runway, or no event to occur.
            <br />
            <br />
            Game Mechanics:
            <ul>
              <li>
                <b>Auction:</b> Reveal top card of deck, starting with current
                player, they may put any number of cards face down on top of it,
                then each player clockwise will take turns putting any number of
                cards down. This continues until everyone chooses not to put
                anymore cards down
              </li>
              <li>
                <b>Runway:</b> take top 4 cards and display them face
                up, the starting player picks one and puts it in their hand.
                This continues for every player clockwise
              </li>
              <li>
                <b>Thrifting:</b> player can discard a card once per turn.
                Rotating clockwise, each player may choose to take that card and
                place it in their hand. If no player takes the card, it is
                discarded
              </li>
              <li>
                <b>Trading:</b> Once a turn, a player may privately offer cards
                with another person at the table. The other player may choose to
                decline and show no cards or offer cards in exchange. The
                exchange happens when both players agree on a trade
              </li>
            </ul>
          </p>
        </Col>
        <Col xs={3} />
      </Row>
      <Row
        style={{ padding: "0px 20px", marginTop: "20px" }}
        className="GameDesign-row"
      >
        <Col xs={3} />
        <Col xs={6}>
          <h2
            className="GameDesign-page-text"
            style={{
              fontSize: "32px",
              textAlign: "center",
              marginBottom: "20px",
            }}
          >
            First Playtest Notes:
          </h2>
          <p className="GameDesign-page-text">
          <ul>
              <li>
                Do nothing die option isn’t fun, make it less likely to happen.
              </li>
              <li>
                Visual sheet to explain scoring, minimal or no text 
              </li>
              <li>
                Visual language and understanding of cards is intuitive
              </li>
              <li>
                Trading mechanic needs to be reworked
              </li>
            </ul>
          </p>
        </Col>
        <Col xs={3} />
      </Row>
      <Row
        style={{
          padding: "0px 200px",
          marginTop: "60px",
          marginBottom: "80px",
        }}
        className="GameDesign-row"
      >
        <Col xs={2} />
        <Col xs={4}>
          <img className="GameDesign-img" src={img5} />
        </Col>
        <Col xs={4}>
          <img className="GameDesign-img" src={img4} />
        </Col>
        <Col xs={2} />
      </Row>
      
      <Row style={{ marginBottom: "80px" }}>
        <Col xs={1} />
        <Col xs={3}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/narrative-design");
            }}
          >
            ← NARRATIVE GAMES
          </div>
        </Col>
        <Col xs={4} />
        <Col xs={4}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/unreal");
            }}
          >
            UNREAL RENDER →
          </div>
        </Col>
      </Row>
    </motion.div>
  );
};

export default GameDesign;
