import "../../BackgroundSolid/styles.css";
import Navbar from "../../Navbar/Navbar"
import {motion} from 'framer-motion'
import "./styles.css"
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import img1 from '../../assets/Arch480/Cover.jpg'
import img2 from '../../assets/Arch480/pictureB.jpg'
import img3 from '../../assets/Arch480/render5.jpeg'
import img4 from '../../assets/Arch480/render7.jpeg'
import img5 from '../../assets/Arch480/render9.jpeg'
import img6 from '../../assets/Arch480/topDownB.jpg'
import img7 from '../../assets/Arch480/topDownC.jpg'
import { useNavigate } from "react-router-dom";

const Arch480 = () => {
  let navigate = useNavigate();
  return <motion.div 
    className="full-screen"
    initial={{opacity: 0}}
    animate={{opacity: 1}}
    exit={{opacity: 0}}
  >
    <Navbar header1="Houses on the Edge of Caldera:" header2="Unreal Engine Render"/>
    
    <Row style={{marginTop: "125px"}} className="Arch480-row">
      <Col xs={1}/>
      <Col xs={4}>
        <img className="Arch480-img" src={img1}/>
      </Col>
      <Col xs={1}/>
      <Col xs={4}>
        <img className="Arch480-img" src={img2}/>
      </Col>
      <Col xs={1}/>
    </Row>
    <Row style={{padding: "0px 80px", marginTop: "60px"}}>
      <Col xs={4}>
        <img className="Arch480-img-med" src={img3}/>
      </Col>
      <Col xs={4}>
        <img className="Arch480-img-med" src={img4}/>
      </Col>
      <Col xs={4}>
        <img className="Arch480-img-med" src={img5}/>
      </Col>
    </Row>
    <Row style={{marginTop: "100px"}}>
      <img className="Arch480-img-big" src={img6}/>
    </Row>
    <Row style={{marginTop: "100px", marginBottom: "80px"}}>
      <img className="Arch480-img-big" src={img7}/>
    </Row>
    <Row style={{ marginBottom: "80px"}}>
      <Col xs={1}/>
      <Col xs={3}>
        <div role="button" className="navbar-button" onClick={()=>{
          navigate("/projects/game-design")
        }}>
          ← GAME DESIGN
        </div>
      </Col>
      <Col xs={4}/>
      <Col xs={4}>
        <div role="button" className="navbar-button" onClick={()=>{
          navigate("/projects/beacon")
        }}>
          BEACON: A LANTERN →
        </div>
      </Col>
    </Row>
  </motion.div>
}

export default Arch480;