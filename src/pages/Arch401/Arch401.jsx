import "../../BackgroundSolid/styles.css";
import Navbar from "../../Navbar/Navbar"
import {motion} from 'framer-motion'
import "./styles.css"
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import img1 from '../../assets/Arch401/Library.png'
import img2 from '../../assets/Arch401/sectionRender.jpg'
import img3 from '../../assets/Arch401/img3.jpg'
import img4 from '../../assets/Arch401/FloorPlans.png'
import img5 from '../../assets/Arch401/img5.png'
import { useNavigate } from "react-router-dom";


const Arch401 = () => {
  let navigate = useNavigate();
  return <motion.div 
    className="full-screen"
    initial={{opacity: 0}}
    animate={{opacity: 1}}
    exit={{opacity: 0}}
  >
    <Navbar header1="Sculpted Curiosity: A Persistent"/>
    
    <Row style={{marginTop: "125px"}} className="Arch401-row">
      <Col xs={1}/>
      <Col xs={4}>
        <img className="Arch401-img" src={img1}/>
      </Col>
      <Col xs={1}/>
      <Col xs={4}>
        <p className="Arch401-page-text">
          A learning facility for children ages 3-5, this building in Rainier View combines two separate community activities into one shared lot. A tabletop gaming store is attached to the building, with a backroom for organized play.
        </p>
      </Col>
      <Col xs={1}/>
    </Row>
    <Row className="Arch401-row">
      <Col xs={1}/>
      <Col xs={4}>
        <p className="Arch401-page-text">
          The learning facility is distinguished by a tight entry point, expanding into a large courtyard open to the sun throughout the year. A large colonnade lines the interior of the building, with large folding doors that can either shelter or expand the space. Classrooms, kitchens, a dynamism, and a library are all programmed into the building.
        </p>
      </Col>
      <Col xs={1}/>
      <Col xs={4}>
        <img className="Arch401-img" src={img2}/>
      </Col>
      <Col xs={1}/>
    </Row>
    <Row className="Arch401-row">
      <Col xs={1}/>
      <Col xs={4}>
        <img className="Arch401-img" src={img3}/>
      </Col>
      <Col xs={1}/>
      <Col xs={4}>
        <p className="Arch401-page-text">
        The administration and game store are single-story, which means that the upper story of the learning center can extend with an inhabitable roof of the other ends of the site. Additionally, this provides an unrestricted view of the light rail and landscape from above.
        </p>
      </Col>
      <Col xs={1}/>
    </Row>
    <Row style={{marginTop: "100px"}}>
      <img className="Arch401-img-big" src={img4}/>
    </Row>
    <Row style={{marginTop: "100px", marginBottom: "80px"}}>
      <img className="Arch401-img-mid" src={img5}/>
    </Row>
    <Row style={{ marginBottom: "80px"}}>
      <Col xs={1}/>
      <Col xs={3}>
        <div role="button" className="navbar-button" onClick={()=>{
          navigate("/projects/food-center")
        }}>
          ← URBAN FOOD CENTER
        </div>
      </Col>
      <Col xs={4}/>
      <Col xs={4}>
        <div role="button" className="navbar-button" onClick={()=>{
          navigate("/projects/personal-work")
        }}>
          PERSONAL PROJECTS →
        </div>
      </Col>
    </Row>
  </motion.div>
}

export default Arch401;