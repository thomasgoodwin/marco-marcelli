import "../../BackgroundSolid/styles.css";
import Navbar from "../../Navbar/Navbar";
import { motion } from "framer-motion";
import "./styles.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useNavigate } from "react-router-dom";
import map2process1 from "../../assets/NarrativeDesign/map2process1.JPG"
import map2process2 from "../../assets/NarrativeDesign/map2process2.JPG"
import map2process3 from "../../assets/NarrativeDesign/map2process3.JPG"
import map2process4 from "../../assets/NarrativeDesign/map2process4.JPG"

const YoutubeVideo = ({videoId, style}) => {
  return <div className="video-responsive center" style={style}>
    <iframe
      width="853"
      height="480"
      src={`https://www.youtube.com/embed/${videoId}`}
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      title="Embedded youtube"
      style={{ display: "block" }}
    />
  </div>
}

const NarrativeDesign = () => {
  let navigate = useNavigate();
  return (
    <motion.div
      className="full-screen"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <Navbar header1="Narrative Level Design in Unreal" style={{marginBottom: "50px"}} />
      <div style={{maxWidth: "1250px", marginBottom: "50px", padding: "0px 15px"}} className="center">
        <h4 className="game-title-text">Enigma: A Narrative Mystery</h4>
        <p className="NarrativeDesign-page-text" style={{marginTop: "10px"}}>
          A coastal area for a narrative game. You are looking for an engineer who has gone missing, finding their abandoned 
          shelter. Environmental clues help you piece together how they’ve been sustaining themselves, as well as where they 
          may have gone. You interact with objects and explore both levels of the shelter to then continue your journey 
          towards the mining rig. The makeshift barricade implies a sense of danger or paranoia from the person living here.
        </p>
      </div>
      <YoutubeVideo videoId="CMyq9XbeaAU" />
      <YoutubeVideo videoId="uqYo6U2ISsA" />
      <div style={{maxWidth: "1250px", marginBottom: "50px", padding: "0px 15px"}} className="center">
        <h4 className="game-title-text">Archaic Ambitions: A Narrative Adventure Game</h4>
        <p className="NarrativeDesign-page-text" style={{marginTop: "10px"}}>
          Mountainous region for a narrative adventure game. Your goal for the level is to find an ancient temple on top of 
          the area, which requires vertical progression. Using climbing mechanics, you have the option to scale the cliff in 
          front, climb on top of the mineshaft, or explore down into the mines. Some collectibles can be found in the 
          barrels, while a secret quest awaits in the mine.
        </p>
      </div>
      <YoutubeVideo videoId="dq74F38Orfo" />
      <YoutubeVideo videoId="5AXex3o2IvQ" />
      <Row style={{maxWidth: "80%" , margin: "20px auto 50px auto"}}>
        <Col>
          <img src={map2process1} alt="" className="NarrativeDesign-img" />
        </Col>
        <Col>
          <img src={map2process2} alt="" className="NarrativeDesign-img"/>
        </Col>
      </Row>
      <Row style={{maxWidth: "80%", margin: "50px auto"}} >
        <Col>
          <img src={map2process3} alt="" className="NarrativeDesign-img"/>
        </Col>
        <Col>
          <img src={map2process4} alt="" className="NarrativeDesign-img"/>
        </Col>
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <Col xs={1} />
        <Col xs={3}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/personal-work");
            }}
          >
            ← PERSONAL PROJECTS
          </div>
        </Col>
        <Col xs={4} />
        <Col xs={4}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/game-design");
            }}
          >
            GAME DESIGN →
          </div>
        </Col>
      </Row>
    </motion.div>
  );
};

export default NarrativeDesign;
