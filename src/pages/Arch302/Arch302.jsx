import "../../BackgroundSolid/styles.css";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Navbar from "../../Navbar/Navbar";
import "./styles.css";

import doc2 from "../../assets/Arch302/pdf/Marcelli_P6-02.jpg";
import doc3 from "../../assets/Arch302/pdf/Marcelli_P6-03.jpg";
import doc4 from "../../assets/Arch302/pdf/Marcelli_P6-04.jpg";
import doc5 from "../../assets/Arch302/pdf/Marcelli_P6-05.jpg";
import doc6 from "../../assets/Arch302/pdf/Marcelli_P6-06.jpg";
import doc7 from "../../assets/Arch302/pdf/Marcelli_P6-07.jpg";
import doc8 from "../../assets/Arch302/pdf/Marcelli_P6-08.jpg";
import doc9 from "../../assets/Arch302/pdf/Marcelli_P6-09.jpg";
import doc10 from "../../assets/Arch302/pdf/Marcelli_P6-10.jpg";
import doc11 from "../../assets/Arch302/pdf/Marcelli_P6-11.jpg";
import doc12 from "../../assets/Arch302/pdf/Marcelli_P6-12.jpg";
import doc13 from "../../assets/Arch302/pdf/Marcelli_P6-13.jpg";
import doc14 from "../../assets/Arch302/pdf/Marcelli_P6-14.jpg";
import doc15 from "../../assets/Arch302/pdf/Marcelli_P6-15.jpg";
import doc16 from "../../assets/Arch302/pdf/Marcelli_P6-16.jpg";
import doc17 from "../../assets/Arch302/pdf/Marcelli_P6-17.jpg";

const Arch302 = () => {
  let navigate = useNavigate();
  return (
    <motion.div
      className="full-screen"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <Navbar header1="Urban Food Center" />
      <Row style={{ margin: "0px auto" }} className="Arch302-row">
        <p
          style={{ maxWidth: "1000px", marginTop: "75px"}}
          className="Arch302-page-text"
        >
          Designed for Columbia City, the urban food center uses a hydroponic
          system to collect rainwater in a large central column hanging from the
          ceiling. Water is transferred to plants covering the building, growing
          on the ledges left from the fractal design of the structure. 
          <br/>
          <br/>
          These extruding shapes lead to activity, a climbing wall/staircase located
          on the exterior of the building for people to climb up to and enjoy
          the view and weather outside while they eat or hold classes. On the
          interior, these ledges are inhabitable. A circulating bridge wraps
          around the floors and central pillar for people to explore the
          verticality of the inside.
        </p>
      </Row>
      <h2
        className="Arch302-page-text"
        style={{
          fontSize: "48px",
          textAlign: "center",
          marginTop: "25px",
          marginBottom: "45px",
        }}
      >
        Design Document
      </h2>
      <Row style={{ marginTop: "80px", marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc2} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc3} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc4} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc5} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc6} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc7} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc8} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc9} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc10} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc11} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc12} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc13} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc14} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc15} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc16} />
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <img className="Arch302-img-big" src={doc17} />
      </Row>

      <Row style={{ marginBottom: "80px" }}>
        <Col xs={1} />
        <Col xs={3}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/beacon");
            }}
          >
            ← BEACON: A LANTERN
          </div>
        </Col>
        <Col xs={4} />
        <Col xs={4}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/sculpted-curiosity");
            }}
          >
            SCULPTED CURIOSITY →
          </div>
        </Col>
      </Row>
    </motion.div>
  );
};

export default Arch302;
