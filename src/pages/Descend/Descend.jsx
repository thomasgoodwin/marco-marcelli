import "../../BackgroundSolid/styles.css";
import Navbar from "../../Navbar/Navbar";
import { motion } from "framer-motion";
import "./styles.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import whiteboxLevel from "../../assets/Descend/Descend.jpg"

import { useNavigate } from "react-router-dom";

const DescendPrototype = () => {
  let navigate = useNavigate();
  return (
    <motion.div
      className="full-screen"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <Navbar header1="Descend: A 2D Map Prototype" />
      <img className="center" style={{maxWidth: "650px", margin: "50px auto 0px auto", padding: "0px 15px"}} src={whiteboxLevel} alt="A whitebox level"/>
      <div style={{maxWidth: "1250px", marginBottom: "150px", padding: "0px 15px", marginTop: "100px"}} className="center">
        <p className="Descend-page-text" >
          Attacks and Defenders fight for control over an underground passageway that provides quick rotation 
          between A and B. Designed with formalist principles, not following strict architectural principles. 
          Intended for gameplay and not world immersion.
        </p>
      </div>
      <Row style={{ marginBottom: "80px" }}>
        <Col xs={1} />
        <Col xs={3}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/personal-work");
            }}
          >
            ← PERSONAL WORK
          </div>
        </Col>
        <Col xs={4} />
        <Col xs={4}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/narrative-design");
            }}
          >
            NARRATIVE GAMES →
          </div>
        </Col>
      </Row>
    </motion.div>
  );
};

export default DescendPrototype;