import "../../BackgroundSolid/styles.css";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Navbar from "../../Navbar/Navbar";
import img1 from "../../assets/PersonalWork/Genesis.jpg";
import img2 from "../../assets/PersonalWork/Repetition.jpg";
import img3 from "../../assets/PersonalWork/Solace.jpg";
import col1 from "../../assets/PersonalWork/Collages/c1.jpg";
import col2 from "../../assets/PersonalWork/Collages/c2.jpg";
import col3 from "../../assets/PersonalWork/Collages/c3.jpg";
import col4 from "../../assets/PersonalWork/Collages/c4.jpg";
import "./styles.css";

const PersonalWork = () => {
  let navigate = useNavigate();
  return (
    <motion.div
      className="full-screen"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <Navbar header1="Personal Work" />
      <Row
        style={{ padding: "0px 20px", marginTop: "125px" }}
        className="PersonalWork-row"
      >
        <img
          style={{ width: "500px" }}
          className="PersonalWork-img"
          src={img1}
        />
        <div style={{ textAlign: "center" }} className="PersonalWork-page-text">
          “Genesis” - 2020, Resin, Mica Powder, Ink, Spray Paint
        </div>
      </Row>
      <Row
        style={{ padding: "0px 20px", marginTop: "125px" }}
        className="PersonalWork-row"
      >
        <img
          style={{ width: "500px" }}
          className="PersonalWork-img"
          src={img3}
        />
        <div style={{ textAlign: "center" }} className="PersonalWork-page-text">
          “Solace” - 2019, Acrylic
        </div>
      </Row>
      <Row
        style={{ padding: "0px 20px", marginTop: "125px" }}
        className="PersonalWork-row"
      >
        <img
          style={{ width: "500px" }}
          className="PersonalWork-img"
          src={img2}
        />
        <div style={{ textAlign: "center" }} className="PersonalWork-page-text">
          “Repetition” - 2018, Wood
        </div>
      </Row>
      <h2
        className="PersonalWork-page-text"
        style={{
          fontSize: "48px",
          textAlign: "center",
          marginTop: "125px",
          marginBottom: "45px",
        }}
      >
        Collages
      </h2>
      <Row
        style={{ marginLeft: "25%" }}
        className="PersonalWork-row"
      >
        <Col xs={4}>
          <img
            style={{ maxWidth: "300px" }}
            className="PersonalWork-img"
            src={col1}
          />
        </Col>
        <Col xs={4}>
          <img
            style={{ maxWidth: "300px" }}
            className="PersonalWork-img"
            src={col2}
          />
        </Col>
      </Row>
      <Row
        style={{ marginTop: "125px", marginBottom: "80px", marginLeft: "25%" }}
        className="PersonalWork-row"
      >
        <Col xs={4}>
          <img
            style={{ maxWidth: "300px" }}
            className="PersonalWork-img"
            src={col3}
          />
        </Col>
        <Col xs={4}>
          <img
            style={{ maxWidth: "300px" }}
            className="PersonalWork-img"
            src={col4}
          />
        </Col>
      </Row>
      <Row style={{ marginBottom: "80px" }}>
        <Col xs={1} />
        <Col xs={3}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/sculpted-curiosity");
            }}
          >
            ← SCULPTED CURIOSITY
          </div>
        </Col>
        <Col xs={4} />
        <Col xs={4}>
          <div
            role="button"
            className="navbar-button"
            onClick={() => {
              navigate("/projects/narrative-design");
            }}
          >
            NARRATIVE GAMES  →
          </div>
        </Col>
      </Row>
    </motion.div>
  );
};

export default PersonalWork;
