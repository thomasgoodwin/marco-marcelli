import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { useNavigate } from "react-router-dom";
import {motion} from 'framer-motion'
import "../BackgroundSolid/styles.css"
import "./styles.css"

const About = () => {
  let navigate = useNavigate();
  return (
    <motion.div 
      className="full-screen"
      initial={{opacity: 0}}
      animate={{opacity: 1}}
      exit={{opacity: 0}}
    >
      <Row>
        <Col xs={10}>
        <div id="about-header">ABOUT</div>
        </Col>
        <Col xs={2}>
          <div id="projects-button" onClick={()=>{
            navigate("/projects")
          }}> PROJECTS</div>
        </Col>
      </Row>
      
      <p id="about-info">
        I am a student at the University of Washington, graduating in June 2022 with a Bachelor’s Degree in Architectural Design. <br/><br/>
        The intersection between architectural and game design is a point where user experience, accessibility, and enjoyment all collide. The studio environment was crucial to my success in the program, giving and receiving constructive feedback from the people I am working in proximity with. Different backgrounds and personal experiences influence how we see the world and design, an important part of communication between designers.<br/><br/> 
        The ability to quickly iterate designs while also being ready to drop them and start again is my educational foundation, a skill I can apply no matter the field. Architecture is experimental, much like games, and the personal experience of using them is the highest priority. 
      </p>
    </motion.div>
  )
}

export default About;