import "./styles.css"

const BackgroundSolid = (props)=> {
  return <div className="full-screen" style={{
    background: `${props.color}`,
  }}/>
}

export default BackgroundSolid;