import { useNavigate } from "react-router-dom";
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import "./styles.css"

const Navbar = ({header1, header2, style}) =>{
  let navigate = useNavigate();

  return <div style={{marginTop: "70px", ...style}}>
    <Row>
      <Col style={{marginLeft: "25px", marginRight:"135px"}} xs={7}>
        <div id="beacon-header">{header1} <br/>{header2}</div>
      </Col>
      <Col xs={2}>
        <div role="button" className="navbar-button" onClick={()=>{
          navigate("/projects")
        }}>
          PROJECTS
        </div>
      </Col>
      <Col xs={1}>
        <div role="button" className="navbar-button" onClick={()=>{
          navigate("/about")
        }}>
          ABOUT
        </div>
      </Col>
    </Row>
  </div>
}

export default Navbar;