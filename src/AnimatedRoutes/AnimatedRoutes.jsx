import React, { Suspense } from 'react';
import App from '../app';
import Projects from '../Projects/Projects';
import About from "../About/About"
import {
  Routes,
  Route,
  useLocation
} from "react-router-dom";
import {AnimatePresence} from 'framer-motion'


const pages = [
  {
    name: "Arch400",
    path: "/beacon",
    Component: React.lazy(()=>import('../pages/Arch400/Arch400')),
  },
  {
    name: "Arch480",
    path: "/unreal",
    Component: React.lazy(()=>import('../pages/Arch480/Arch480')),
  },
  {
    name: "Descend: A 2D Map Prototype",
    path: "/descend",
    Component: React.lazy(()=>import('../pages/Descend/Descend')),
  },
  {
    name: "Narrative Level Design in Unreal",
    path: "/narrative-design",
    Component: React.lazy(()=>import('../pages/NarrativeDesign/NarrativeDesign')),
  },
  {
    name: "Game Design",
    path: "/game-design",
    Component: React.lazy(()=>import('../pages/GameDesign/GameDesign')),
  },
  {
    name: "Urban Food Center",
    path: "/food-center",
    Component: React.lazy(()=>import('../pages/Arch302/Arch302')),
  },
  {
    name: "Sculpted Curiosity: A Persistent",
    path: "/sculpted-curiosity",
    Component: React.lazy(()=>import('../pages/Arch401/Arch401')),
  },
  {
    name: "Personal Work",
    path: "/personal-work",
    Component: React.lazy(()=>import('../pages/PersonalWork/PersonalWork')),
  },
]

const AnimatedRoutes = () => {
  const location = useLocation();

  return (
    <AnimatePresence>
      <Routes location={location} key={location.pathname}>
        {pages.map(({path, Component}) => {
          return <Route key={path} exact path={"/projects" + path} element={
            <Suspense fallback={
              <div className="full-screen">
              </div>
            }>
              <Component/>
            </Suspense>
          }/>
        })}
        <Route path="/" element={<App/>}/>
        <Route path="/projects/" element={<Projects/>}/>
        <Route path="/about/" element={<About/>}/>
      </Routes>
    </AnimatePresence>
  )
}

export default AnimatedRoutes;