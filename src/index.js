import React from 'react';
import ReactDOM from 'react-dom';
import AnimatedRoutes from './AnimatedRoutes/AnimatedRoutes';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./BackgroundSolid/styles.css"
import {
  BrowserRouter,
} from "react-router-dom";

ReactDOM.render(
  <BrowserRouter>
    <AnimatedRoutes/>
  </BrowserRouter>,
  document.getElementById('root')
);
